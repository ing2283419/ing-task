package com.ing.task.transactions.service;

import static com.ing.task.transactions.service.TransactionsService.createReport;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.ing.task.transactions.dto.input.TransactionInputDTO;
import com.ing.task.transactions.dto.output.TransactionOutputDTO;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import org.junit.jupiter.api.Test;

@MicronautTest
class TransactionsServiceTest {

  public final static String ACCOUNT_1 = "PL221426678132881690262851";
  public final static String ACCOUNT_2 = "PL010230883119350401235533";
  public final static String ACCOUNT_3 = "UK447353517965712896741083";
  public final static String ACCOUNT_4 = "PL116892567837299641911501";
  public final static String ACCOUNT_5 = "PL262727498027036604335704";
  public final static String ACCOUNT_6 = "DE114735990046191452077193";
  public final static String ACCOUNT_7 = "DE514735990046191452077193";

  @Test
  void createReportShouldReturnEmptyListIfInputIsNull() {
    // given/when
    Collection<TransactionOutputDTO> result = createReport(null);
    // then
    assertTrue(result.isEmpty());
  }

  @Test
  void createReportShouldReturnEmptyListIfInputIsEmpty() {
    // given/when
    Collection<TransactionOutputDTO> result = createReport(emptyList());
    // then
    assertTrue(result.isEmpty());
  }

  @Test
  void createReportShouldReturnCorrectData() {
    // given
    List<TransactionInputDTO> transactions = prepareTestData();
    //when
    List<TransactionOutputDTO> result = createReport(transactions).stream().toList();
    // then
    assertEquals(7, result.size());
    assertTransactionOutput(result.get(0), "DE114735990046191452077193", 2, 2, valueOf(8.04));
    assertTransactionOutput(result.get(1), "DE514735990046191452077193", 1, 1, ZERO);
    assertTransactionOutput(result.get(2), "PL010230883119350401235533", 4, 7, valueOf(113.78));
    assertTransactionOutput(result.get(3), "PL116892567837299641911501", 3, 3, valueOf(-71.27));
    assertTransactionOutput(result.get(4), "PL221426678132881690262851", 7, 4, valueOf(-37.07));
    assertTransactionOutput(result.get(5), "PL262727498027036604335704", 3, 4, valueOf(-4.19));
    assertTransactionOutput(result.get(6), "UK447353517965712896741083", 2, 1, valueOf(-9.29));
  }

  private static List<TransactionInputDTO> prepareTestData() {
    return List.of(
        new TransactionInputDTO(ACCOUNT_1, ACCOUNT_2, valueOf(10.23)),
        new TransactionInputDTO(ACCOUNT_2, ACCOUNT_1, valueOf(76.23)),
        new TransactionInputDTO(ACCOUNT_3, ACCOUNT_2, valueOf(-2.23)), // pozwalamy na ujemne operacje — zakładam, że tak
        new TransactionInputDTO(ACCOUNT_1, ACCOUNT_2, valueOf(10.07)),
        new TransactionInputDTO(ACCOUNT_1, ACCOUNT_1, valueOf(13.22)),
        new TransactionInputDTO(ACCOUNT_5, ACCOUNT_2, valueOf(53.51)),
        new TransactionInputDTO(ACCOUNT_1, ACCOUNT_4, valueOf(52.68)),
        new TransactionInputDTO(ACCOUNT_6, ACCOUNT_5, valueOf(49.87)),
        new TransactionInputDTO(ACCOUNT_2, ACCOUNT_1, valueOf(63.44)),
        new TransactionInputDTO(ACCOUNT_3, ACCOUNT_2, valueOf(94.76)),
        new TransactionInputDTO(ACCOUNT_5, ACCOUNT_4, valueOf(38.04)),
        new TransactionInputDTO(ACCOUNT_1, ACCOUNT_2, valueOf(98.29)),
        new TransactionInputDTO(ACCOUNT_2, ACCOUNT_4, valueOf(30.92)),
        new TransactionInputDTO(ACCOUNT_4, ACCOUNT_3, valueOf(83.24)),
        new TransactionInputDTO(ACCOUNT_1, ACCOUNT_5, valueOf(74.33)),
        new TransactionInputDTO(ACCOUNT_1, ACCOUNT_5, valueOf(18.30)),
        new TransactionInputDTO(ACCOUNT_4, ACCOUNT_2, valueOf(77.65)),
        new TransactionInputDTO(ACCOUNT_6, ACCOUNT_6, valueOf(13.41)),
        new TransactionInputDTO(ACCOUNT_2, ACCOUNT_6, valueOf(57.91)),
        new TransactionInputDTO(ACCOUNT_4, ACCOUNT_5, valueOf(32.02)),
        new TransactionInputDTO(ACCOUNT_5, ACCOUNT_1, valueOf(87.16)),
        new TransactionInputDTO(ACCOUNT_7, ACCOUNT_7, valueOf(23.67)) // pozwalamy na transakcje na to samo kontoa - zakładam, że tak
    );
  }

  private static void assertTransactionOutput(final TransactionOutputDTO actual, final String account, final long debitCount, final long creditCount, final BigDecimal balance) {
    assertNotNull(actual);
    assertEquals(account, actual.getAccount());
    assertEquals(debitCount, actual.getDebitCount());
    assertEquals(creditCount, actual.getCreditCount());
    assertEquals(balance.doubleValue(), actual.getBalance().doubleValue());
  }

}