package com.ing.task.atm.dto.output;

import io.micronaut.serde.annotation.Serdeable;

@Serdeable
public record ServiceTaskOutputDTO(
    int region,
    int atmId
) {

}