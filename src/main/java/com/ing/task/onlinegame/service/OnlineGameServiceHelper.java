package com.ing.task.onlinegame.service;

import static com.ing.task.onlinegame.service.OnlineGameValidationService.validateClansCount;
import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toCollection;

import com.ing.task.onlinegame.dto.helper.OnlineGameClansWithCountDTO;
import com.ing.task.onlinegame.dto.input.OnlineGameClanDTO;
import com.ing.task.onlinegame.dto.output.OnlineGameOutputDTO;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

class OnlineGameServiceHelper {

  private final static int ZERO = 0;

  private static final Comparator<OnlineGameClanDTO> ONLINE_GAME_CLAN_DTO_COMPARATOR = comparing(
      OnlineGameClanDTO::getPoints, reverseOrder()
  ).thenComparing(
      OnlineGameClanDTO::getNumberOfPlayers
  );

  /**
   * Metoda usuwa z listy wszystkie klany, których ilość graczy przekracza rozmiar grupy.
   * Następnie grupuje klany na podstawie ilość graczy oraz ilość punktów i zlicza ilość takich klanów.
   * Zwracana lista klanów jest posortowana w kolejności od najmocniejszych klanów, do najsłabszych.
   *
   * @param inputClans    - klany do przetworzenia
   * @param maxGroupCount - maksymalna wielkość grupy
   * @return posortowana lista klanów wraz z ich liczebnością (ilość klanów z tą samą ilością graczy i punktów)
   */
  static List<OnlineGameClansWithCountDTO> validateAndGroupClans(final List<OnlineGameClanDTO> inputClans, final int maxGroupCount) {
    validateClansCount(inputClans.size());
    return inputClans
        .parallelStream()
        .peek(OnlineGameValidationService::validateOnlineGameClan)
        .filter(clan -> clan.getNumberOfPlayers() <= maxGroupCount)
        .collect(groupingBy(identity(), counting()))
        .entrySet()
        .stream()
        .map(e -> new OnlineGameClansWithCountDTO(e.getKey(), e.getValue()))
        .sorted(ONLINE_GAME_CLAN_DTO_COMPARATOR)
        .collect(toCollection(LinkedList::new));
  }

  static boolean groupIsNotFull(final int remainingPlaceInGroup) {
    return remainingPlaceInGroup != ZERO;
  }

  static boolean canAddClanToGroup(final int numberOfPlayers, final int remainingPlaceInGroup) {
    return numberOfPlayers <= remainingPlaceInGroup;
  }

  static void addClanToGroup(final List<OnlineGameOutputDTO> clansGroup, final int numberOfPlayers, final int points) {
    clansGroup.add(new OnlineGameOutputDTO(numberOfPlayers, points));
  }

}
