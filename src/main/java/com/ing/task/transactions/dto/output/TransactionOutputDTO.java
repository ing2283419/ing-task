package com.ing.task.transactions.dto.output;

import static java.math.BigDecimal.ZERO;

import io.micronaut.serde.annotation.Serdeable;
import java.math.BigDecimal;

@Serdeable
public class TransactionOutputDTO {

  private final String account;
  private int debitCount = 0;
  private int creditCount = 0;
  private BigDecimal balance = ZERO;

  public TransactionOutputDTO(final String account) {
    this.account = account;
  }

  public String getAccount() {
    return account;
  }

  public int getDebitCount() {
    return debitCount;
  }

  public int getCreditCount() {
    return creditCount;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void addToBalance(final BigDecimal value) {
    this.balance = this.balance.add(value);
  }

  public void subtractFromBalance(final BigDecimal value) {
    this.balance = this.balance.subtract(value);
  }

  public void incrementDebitCount() {
    this.debitCount++;
  }

  public void incrementCreditCount() {
    this.creditCount++;
  }

}
