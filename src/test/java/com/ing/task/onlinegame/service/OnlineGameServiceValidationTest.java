package com.ing.task.onlinegame.service;

import static com.ing.task.onlinegame.service.OnlineGameService.calculateGroupsOrder;
import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.ing.task.onlinegame.dto.input.OnlineGameClanDTO;
import com.ing.task.onlinegame.dto.input.OnlineGameInputDTO;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import java.util.Arrays;
import org.junit.jupiter.api.Test;

@MicronautTest
class OnlineGameServiceValidationTest {

  @Test
  void calculateGroupsOrderShouldThrowExceptionMaxGroupCountLowerThan1() {
    // given
    final OnlineGameInputDTO players = new OnlineGameInputDTO(0, of(new OnlineGameClanDTO(1, 1)));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> calculateGroupsOrder(players));
    // then
    assertEquals("The minimum group size is 1.", thrown.getMessage());
  }

  @Test
  void calculateGroupsOrderShouldThrowExceptionMaxGroupCountBiggerThan1000() {
    // given
    final OnlineGameInputDTO players = new OnlineGameInputDTO(1_001, of(new OnlineGameClanDTO(1, 1)));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> calculateGroupsOrder(players));
    // then
    assertEquals("The maximum group size is 1000.", thrown.getMessage());
  }

  @Test
  void calculateGroupsOrderShouldThrowExceptionMoreThan20_000Clans() {
    // given
    final OnlineGameInputDTO players = new OnlineGameInputDTO(1, Arrays.asList(new OnlineGameClanDTO[20_001]));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> calculateGroupsOrder(players));
    // then
    assertEquals("The maximum number of clans is 20000.", thrown.getMessage());
  }

  @Test
  void calculateGroupsOrderShouldThrowExceptionNumberOfPlayersInClanSmallerThan1() {
    // given
    final OnlineGameInputDTO players = new OnlineGameInputDTO(1, of(new OnlineGameClanDTO(0, 1_000_000)));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> calculateGroupsOrder(players));
    // then
    assertEquals("The minimum clan size is 1.", thrown.getMessage());
  }

  @Test
  void calculateGroupsOrderShouldThrowExceptionNumberOfPlayersInClanBiggerThan1000() {
    // given
    final OnlineGameInputDTO players = new OnlineGameInputDTO(1000, of(new OnlineGameClanDTO(1001, 1)));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> calculateGroupsOrder(players));
    // then
    assertEquals("The maximum clan size is 1000.", thrown.getMessage());
  }

  @Test
  void calculateGroupsOrderShouldThrowExceptionClanPointsSmallerThan1() {
    // given
    final OnlineGameInputDTO players = new OnlineGameInputDTO(1, of(new OnlineGameClanDTO(1, 0)));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> calculateGroupsOrder(players));
    // then
    assertEquals("The minimum number of points in the clan is 1.", thrown.getMessage());
  }

  @Test
  void calculateGroupsOrderShouldThrowExceptionClanPointsBiggerThan1_000_000() {
    // given
    final OnlineGameInputDTO players = new OnlineGameInputDTO(1000, of(new OnlineGameClanDTO(1000, 1_000_001)));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> calculateGroupsOrder(players));
    // then
    assertEquals("The maximum number of points in the clan is 1000000.", thrown.getMessage());
  }

}