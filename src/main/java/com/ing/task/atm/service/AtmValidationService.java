package com.ing.task.atm.service;

import com.ing.task.atm.dto.input.RequestType;
import com.ing.task.atm.dto.input.ServiceTaskInputDTO;

public class AtmValidationService {

  private final static int MIN_REGION_NUMBER = 1;
  private static final String MIN_REGION_NUMBER_ERROR_MSG = String.format("The minimum region number is %d.", MIN_REGION_NUMBER);
  private final static int MAX_REGION_NUMBER = 9999;
  private static final String MAX_REGION_NUMBER_ERROR_MSG = String.format("The maximum region number is %d.", MAX_REGION_NUMBER);

  private final static int MIN_ATM_ID = 1;
  private static final String MIN_ATM_ID_ERROR_MSG = String.format("The minimum ATM ID is %d.", MIN_ATM_ID);
  private final static int MAX_ATM_ID = 9999;
  private static final String MAX_ATM_ID_ERROR_MSG = String.format("The maximum ATM ID is %d.", MAX_ATM_ID);

  private static final String REQUEST_TYPE_ERROR_MSG = "The request type should be other than null.";

  /**
   * Metoda sprawdza czy dane zgłoszenie jest poprawne.
   * Jeżeli RequestType jest nieustawiony, zostanie rzucony wyjątek.
   * Jeżeli numer regionu nie mieści się w określonym zakresie, zostanie rzucony wyjątek.
   * Jeżeli ID bankomatu (atmId) nie mieści się w określonym zakresie, zostanie rzucony wyjątek.
   *
   * @param serviceTaskInput - zgłoszenie do sprawdzenia.
   */
  static void validateServiceTaskInput(ServiceTaskInputDTO serviceTaskInput) {
    validateRequestType(serviceTaskInput.getRequestType());
    validateRegionNumber(serviceTaskInput.getRegion());
    validateAtmId(serviceTaskInput.getAtmId());
  }

  private static void validateRequestType(final RequestType requestType) {
    if (requestType == null) {
      throw new RuntimeException(REQUEST_TYPE_ERROR_MSG);
    }
  }

  private static void validateRegionNumber(final int region) {
    if (region < MIN_REGION_NUMBER) {
      throw new RuntimeException(MIN_REGION_NUMBER_ERROR_MSG);
    }
    if (region > MAX_REGION_NUMBER) {
      throw new RuntimeException(MAX_REGION_NUMBER_ERROR_MSG);
    }
  }

  private static void validateAtmId(final int atmId) {
    if (atmId < MIN_ATM_ID) {
      throw new RuntimeException(MIN_ATM_ID_ERROR_MSG);
    }
    if (atmId > MAX_ATM_ID) {
      throw new RuntimeException(MAX_ATM_ID_ERROR_MSG);
    }
  }

}
