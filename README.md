## Zielona Tesla za Zielony kod — rozwiązanie

Aplikacja ma zaimplementowane rozwiązanie trzech zadań zgodnie w wytycznymi znajdującymi się w folderze [zadania](./zadania).

Aplikacja wystawia trzy REST-Pointy, którym zadaniem jest **walidacja danych wejściowych** zgodnie z regułami ze specyfikacji
endpointów w standarcie OpenAPI 3.0 znajdują się w plikach .json oraz **odpowiednie przetworzenie** tych danych.

---

## Technologia

Aplikacja wykorzystuje środowisko **Java 17** oraz narzędzie **Gradle w wersji 8.0.1** do działania. <br/>
Jako podstawa aplikacji został użyty **Micronaut Framework** w wersji 4.0.

## Static Application Security Testing (SAST)

Projekt posiada statyczną analizę bezpieczeństwa, która jest wykonywana przy każdej wrzucanej zmianie kodu do repozytorium.<br/><br/>
Raport można pobrać wchodzą na stronę z [pipelines](https://gitlab.com/ing2283419/ing-task/-/pipelines) i klikając w ikonę po prawej stronie (pojawią się dwie opcje, jedna z raportem SAST, druga z
raportem licencji).<br/>
Dodatkowo raport w postaci .json wrzucony jest do repo, można go znaleźć [tutaj](./sast/gl-sast-report.json).

## Budowanie oraz uruchamianie aplikacji

Aplikację można zbudować używając skryptu [build.sh](./build.sh) lub manualnie wykorzystując narzędzie Gradle. <br/>
Po poprawnym zbudowaniu, wykonywalny plik .jar powinien znajdować się w folderze [/build/libs](./build/libs). Interesuje nas plik [ing-taks-1.0-all.jar](./build/libs/ing-taks-1.0-all.jar).<br/><br/>
Do uruchomienia aplikacji można wykorzystać skrypt [run.sh](./run.sh). Skrypt ten uruchamia wyżej opisany .jar z dodatkowymi parametrami.<br/><br/>

Dodatkowo w folderze [app](./app) znajduję się zbudowany jar, który można bezpośrednio uruchomić. Jest to identyczny jar, który został wbudowany za pomocą skryptu [build.sh](./build.sh).

## Twórca
Mateusz Gocz - msgocz@gmail.com