package com.ing.task.atm.service;

import static com.ing.task.atm.dto.input.RequestType.FAILURE_RESTART;
import static com.ing.task.atm.dto.input.RequestType.PRIORITY;
import static com.ing.task.atm.dto.input.RequestType.SIGNAL_LOW;
import static com.ing.task.atm.dto.input.RequestType.STANDARD;
import static com.ing.task.atm.service.AtmService.calculateOrder;
import static java.util.Collections.emptyList;
import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.ing.task.atm.dto.input.ServiceTaskInputDTO;
import com.ing.task.atm.dto.output.ServiceTaskOutputDTO;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import java.util.List;
import org.junit.jupiter.api.Test;

@MicronautTest
class AtmServiceTest {

  @Test
  void calculateOrderShouldReturnEmptyListIfInputIsNull() {
    // given/when
    List<ServiceTaskOutputDTO> result = calculateOrder(null);
    // then
    assertTrue(result.isEmpty());
  }

  @Test
  void calculateOrderShouldReturnEmptyListIfInputIsEmpty() {
    // given/when
    List<ServiceTaskOutputDTO> result = calculateOrder(emptyList());
    // then
    assertTrue(result.isEmpty());
  }

  @Test
  void calculateOrderShouldReturnCorrectData() {
    // given
    final List<ServiceTaskInputDTO> serviceTasks = prepareTestData();
    //when
    List<ServiceTaskOutputDTO> result = calculateOrder(serviceTasks);
    // then
    assertEquals(14, result.size());

    assertServiceTaskOutput(result.get(0), 1, 4);
    assertServiceTaskOutput(result.get(1), 1, 3);
    assertServiceTaskOutput(result.get(2), 1, 2);
    assertServiceTaskOutput(result.get(3), 1, 1);

    assertServiceTaskOutput(result.get(4), 2, 5);
    assertServiceTaskOutput(result.get(5), 2, 1);
    assertServiceTaskOutput(result.get(6), 2, 4);
    assertServiceTaskOutput(result.get(7), 2, 3);
    assertServiceTaskOutput(result.get(8), 2, 2);

    assertServiceTaskOutput(result.get(9), 3, 3);
    assertServiceTaskOutput(result.get(10), 3, 2);
    assertServiceTaskOutput(result.get(11), 3, 1);

    assertServiceTaskOutput(result.get(12), 4, 3);
    assertServiceTaskOutput(result.get(13), 4, 1);
  }

  private static List<ServiceTaskInputDTO> prepareTestData() {
    return of(
        new ServiceTaskInputDTO(1, 2, STANDARD),
        new ServiceTaskInputDTO(1, 1, STANDARD),
        new ServiceTaskInputDTO(1, 3, STANDARD),
        new ServiceTaskInputDTO(1, 3, PRIORITY),
        new ServiceTaskInputDTO(1, 3, STANDARD),
        new ServiceTaskInputDTO(1, 4, SIGNAL_LOW),
        new ServiceTaskInputDTO(1, 4, FAILURE_RESTART),

        new ServiceTaskInputDTO(2, 5, STANDARD),
        new ServiceTaskInputDTO(2, 5, FAILURE_RESTART),
        new ServiceTaskInputDTO(2, 1, FAILURE_RESTART),
        new ServiceTaskInputDTO(2, 3, STANDARD),
        new ServiceTaskInputDTO(2, 2, STANDARD),
        new ServiceTaskInputDTO(2, 2, STANDARD),
        new ServiceTaskInputDTO(2, 4, SIGNAL_LOW),
        new ServiceTaskInputDTO(2, 4, PRIORITY),
        new ServiceTaskInputDTO(2, 4, SIGNAL_LOW),

        new ServiceTaskInputDTO(3, 1, STANDARD),
        new ServiceTaskInputDTO(3, 2, SIGNAL_LOW),
        new ServiceTaskInputDTO(3, 3, PRIORITY),

        new ServiceTaskInputDTO(4, 1, PRIORITY),
        new ServiceTaskInputDTO(4, 3, FAILURE_RESTART)
    );
  }

  private static void assertServiceTaskOutput(final ServiceTaskOutputDTO actual, final int region, final int atmId) {
    assertNotNull(actual);
    assertEquals(region, actual.region());
    assertEquals(atmId, actual.atmId());
  }

}