package com.ing.task.onlinegame.service;

import static com.ing.task.onlinegame.service.OnlineGameService.calculateGroupsOrder;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.ing.task.onlinegame.dto.input.OnlineGameClanDTO;
import com.ing.task.onlinegame.dto.input.OnlineGameInputDTO;
import com.ing.task.onlinegame.dto.output.OnlineGameOutputDTO;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import java.util.List;
import org.junit.jupiter.api.Test;

@MicronautTest
class OnlineGameServiceTest {

  @Test
  void calculateGroupsOrderShouldReturnEmptyListIfInputIsNull() {
    // given
    final OnlineGameInputDTO players = new OnlineGameInputDTO(1, null);
    // when
    List<List<OnlineGameOutputDTO>> result = calculateGroupsOrder(players);
    // then
    assertTrue(result.isEmpty());
  }

  @Test
  void calculateGroupsOrderShouldReturnEmptyListIfInputIsEmpty() {
    // given
    final OnlineGameInputDTO players = new OnlineGameInputDTO(1, emptyList());
    // when
    List<List<OnlineGameOutputDTO>> result = calculateGroupsOrder(players);
    // then
    assertTrue(result.isEmpty());
  }

  @Test
  void calculateGroupsOrderShouldReturnCorrectDataForGroupCount_1() {
    // given
    final OnlineGameInputDTO players = prepareTestData(1);
    // when
    List<List<OnlineGameOutputDTO>> result = calculateGroupsOrder(players);
    // then
    assertEquals(2, result.size());
    assertEquals(1, result.get(0).size());
    assertOnlineGameOutput(result.get(0).get(0), 1, 5);
    assertEquals(1, result.get(1).size());
    assertOnlineGameOutput(result.get(1).get(0), 1, 4);
  }

  @Test
  void calculateGroupsOrderShouldReturnCorrectDataForGroupCount_4() {
    // given
    final OnlineGameInputDTO players = prepareTestData(4);
    // when
    List<List<OnlineGameOutputDTO>> result = calculateGroupsOrder(players);
    // then
    assertEquals(7, result.size());

    assertEquals(1, result.get(0).size());
    assertOnlineGameOutput(result.get(0).get(0), 4, 67);

    assertEquals(1, result.get(1).size());
    assertOnlineGameOutput(result.get(1).get(0), 4, 67);

    assertEquals(2, result.get(2).size());
    assertOnlineGameOutput(result.get(2).get(0), 3, 54);
    assertOnlineGameOutput(result.get(2).get(1), 1, 5);

    assertEquals(2, result.get(3).size());
    assertOnlineGameOutput(result.get(3).get(0), 2, 50);
    assertOnlineGameOutput(result.get(3).get(1), 2, 50);

    assertEquals(2, result.get(4).size());
    assertOnlineGameOutput(result.get(4).get(0), 2, 50);
    assertOnlineGameOutput(result.get(4).get(1), 2, 23);

    assertEquals(2, result.get(5).size());
    assertOnlineGameOutput(result.get(5).get(0), 3, 23);
    assertOnlineGameOutput(result.get(5).get(1), 1, 4);

    assertEquals(1, result.get(6).size());
    assertOnlineGameOutput(result.get(6).get(0), 3, 23);
  }

  @Test
  void calculateGroupsOrderShouldReturnCorrectDataForGroupCount_5() {
    // given
    final OnlineGameInputDTO players = prepareTestData(5);
    // when
    List<List<OnlineGameOutputDTO>> result = calculateGroupsOrder(players);
    // then
    assertEquals(7, result.size());

    assertEquals(2, result.get(0).size());
    assertOnlineGameOutput(result.get(0).get(0), 4, 67);
    assertOnlineGameOutput(result.get(0).get(1), 1, 5);

    assertEquals(2, result.get(1).size());
    assertOnlineGameOutput(result.get(1).get(0), 4, 67);
    assertOnlineGameOutput(result.get(1).get(1), 1, 4);

    assertEquals(2, result.get(2).size());
    assertOnlineGameOutput(result.get(2).get(0), 3, 54);
    assertOnlineGameOutput(result.get(2).get(1), 2, 50);

    assertEquals(2, result.get(3).size());
    assertOnlineGameOutput(result.get(3).get(0), 2, 50);
    assertOnlineGameOutput(result.get(3).get(1), 2, 50);

    assertEquals(1, result.get(4).size());
    assertOnlineGameOutput(result.get(4).get(0), 5, 45);

    assertEquals(2, result.get(5).size());
    assertOnlineGameOutput(result.get(5).get(0), 2, 23);
    assertOnlineGameOutput(result.get(5).get(1), 3, 23);

    assertEquals(1, result.get(6).size());
    assertOnlineGameOutput(result.get(6).get(0), 3, 23);
  }

  @Test
  void calculateGroupsOrderShouldReturnCorrectDataForGroupCount_7() {
    // given
    final OnlineGameInputDTO players = prepareTestData(7);
    // when
    List<List<OnlineGameOutputDTO>> result = calculateGroupsOrder(players);
    // then
    assertEquals(6, result.size());

    assertEquals(2, result.get(0).size());
    assertOnlineGameOutput(result.get(0).get(0), 6, 79);
    assertOnlineGameOutput(result.get(0).get(1), 1, 5);

    assertEquals(2, result.get(1).size());
    assertOnlineGameOutput(result.get(1).get(0), 4, 67);
    assertOnlineGameOutput(result.get(1).get(1), 3, 54);

    assertEquals(3, result.get(2).size());
    assertOnlineGameOutput(result.get(2).get(0), 4, 67);
    assertOnlineGameOutput(result.get(2).get(1), 2, 50);
    assertOnlineGameOutput(result.get(2).get(2), 1, 4);

    assertEquals(3, result.get(3).size());
    assertOnlineGameOutput(result.get(3).get(0), 2, 50);
    assertOnlineGameOutput(result.get(3).get(1), 2, 50);
    assertOnlineGameOutput(result.get(3).get(2), 2, 23);

    assertEquals(1, result.get(4).size());
    assertOnlineGameOutput(result.get(4).get(0), 5, 45);

    assertEquals(2, result.get(5).size());
    assertOnlineGameOutput(result.get(5).get(0), 3, 23);
    assertOnlineGameOutput(result.get(5).get(1), 3, 23);
  }

  @Test
  void calculateGroupsOrderShouldReturnCorrectDataForGroupCount_1000() {
    // given
    final OnlineGameInputDTO players = prepareTestData(1000);
    // when
    List<List<OnlineGameOutputDTO>> result = calculateGroupsOrder(players);
    // then
    assertEquals(1, result.size());

    assertEquals(16, result.get(0).size());
    assertOnlineGameOutput(result.get(0).get(0), 9, 90);
    assertOnlineGameOutput(result.get(0).get(1), 6, 79);
    assertOnlineGameOutput(result.get(0).get(2), 4, 67);
    assertOnlineGameOutput(result.get(0).get(3), 4, 67);
    assertOnlineGameOutput(result.get(0).get(4), 3, 54);
    assertOnlineGameOutput(result.get(0).get(5), 2, 50);
    assertOnlineGameOutput(result.get(0).get(6), 2, 50);
    assertOnlineGameOutput(result.get(0).get(7), 2, 50);
    assertOnlineGameOutput(result.get(0).get(8), 8, 50);
    assertOnlineGameOutput(result.get(0).get(9), 8, 50);
    assertOnlineGameOutput(result.get(0).get(10), 5, 45);
    assertOnlineGameOutput(result.get(0).get(11), 2, 23);
    assertOnlineGameOutput(result.get(0).get(12), 3, 23);
    assertOnlineGameOutput(result.get(0).get(13), 3, 23);
    assertOnlineGameOutput(result.get(0).get(14), 1, 5);
    assertOnlineGameOutput(result.get(0).get(15), 1, 4);
  }

  private static OnlineGameInputDTO prepareTestData(final int groupCount) {
    return new OnlineGameInputDTO(
        groupCount,
        List.of(
            new OnlineGameClanDTO(2, 50),
            new OnlineGameClanDTO(3, 54),
            new OnlineGameClanDTO(5, 45),
            new OnlineGameClanDTO(1, 4),
            new OnlineGameClanDTO(6, 79),
            new OnlineGameClanDTO(8, 50),
            new OnlineGameClanDTO(2, 23),
            new OnlineGameClanDTO(3, 23),
            new OnlineGameClanDTO(3, 23),
            new OnlineGameClanDTO(2, 50),
            new OnlineGameClanDTO(2, 50),
            new OnlineGameClanDTO(4, 67),
            new OnlineGameClanDTO(4, 67),
            new OnlineGameClanDTO(8, 50),
            new OnlineGameClanDTO(1, 5),
            new OnlineGameClanDTO(9, 90)
        )
    );
  }

  private static void assertOnlineGameOutput(final OnlineGameOutputDTO actual, final int numberOfPlayers, final int points) {
    assertNotNull(actual);
    assertEquals(numberOfPlayers, actual.numberOfPlayers());
    assertEquals(points, actual.points());
  }

}