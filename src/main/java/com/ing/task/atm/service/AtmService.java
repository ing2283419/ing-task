package com.ing.task.atm.service;

import static com.google.common.collect.Streams.mapWithIndex;
import static io.micronaut.core.util.CollectionUtils.isNotEmpty;
import static java.util.Collections.emptyList;
import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;
import static java.util.Comparator.nullsLast;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.maxBy;
import static java.util.stream.Collectors.toList;

import com.ing.task.atm.dto.input.ServiceTaskInputDTO;
import com.ing.task.atm.dto.output.ServiceTaskOutputDTO;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class AtmService {

  private static final Comparator<ServiceTaskInputDTO> ATM_ORDER_COMPARATOR = comparing(
      ServiceTaskInputDTO::getRegion // jako pierwsze sortujemy po regionie
  ).thenComparing(
      ServiceTaskInputDTO::getRequestPrio, reverseOrder() // następnie po rodzaju zlecenia
  ).thenComparing(
      nullsLast(
          comparing(ServiceTaskInputDTO::getOrderNumber) // na końcu po kolejności zgłoszeń
      )
  );

  /**
   * Metoda tworzy listę priorytetową zleceń obsługi bankomatów.
   * Lista jest posortowana po regionach, następnie po prio zlecenia.
   * Jeżeli na liście są bankomaty z tego samego regionu oraz z tym samym
   * zleceniem, pierwszy zostanie obsłużony ten, który pierwszy zgłosił awarię.
   *
   * @param serviceTasks - lista zleceń do posortowania.
   * @return posortowania lista zleceń, od najważniejszych, do najmniej ważnych.
   */
  public static List<ServiceTaskOutputDTO> calculateOrder(final List<ServiceTaskInputDTO> serviceTasks) {
    if (isNotEmpty(serviceTasks)) {
      return orderByRegionAndRequestPrio(
          validateAndFindAtmsWithHighestRequestPrioGroupedByRegionAndId(serviceTasks)
      );
    }
    return emptyList();
  }

  private static List<ServiceTaskOutputDTO> orderByRegionAndRequestPrio(final Stream<ServiceTaskInputDTO> serviceTasks) {
    return serviceTasks.sorted(ATM_ORDER_COMPARATOR)
        .map(AtmService::mapToOutput)
        .collect(toList());
  }

  private static Stream<ServiceTaskInputDTO> validateAndFindAtmsWithHighestRequestPrioGroupedByRegionAndId(final List<ServiceTaskInputDTO> serviceTasks) {
    return mapWithIndex(serviceTasks.stream(), ServiceTaskInputDTO::withOrderNumber)
        .peek(AtmValidationService::validateServiceTaskInput)
        .collect(
            groupingBy(
                ServiceTaskInputDTO::getAtmKey,
                maxBy(
                    comparingInt(
                        ServiceTaskInputDTO::getRequestPrio
                    )
                )
            )
        )
        .values()
        .stream()
        .filter(Optional::isPresent)
        .map(Optional::get);
  }

  private static ServiceTaskOutputDTO mapToOutput(final ServiceTaskInputDTO serviceTask) {
    return new ServiceTaskOutputDTO(serviceTask.getRegion(), serviceTask.getAtmId());
  }

}
