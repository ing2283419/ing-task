package com.ing.task.onlinegame.dto.input;

import io.micronaut.serde.annotation.Serdeable;
import java.util.Objects;

@Serdeable
public class OnlineGameClanDTO {

  private final int numberOfPlayers;
  private final int points;

  public OnlineGameClanDTO(final int numberOfPlayers, final int points) {
    this.numberOfPlayers = numberOfPlayers;
    this.points = points;
  }

  public int getNumberOfPlayers() {
    return numberOfPlayers;
  }

  public int getPoints() {
    return points;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof OnlineGameClanDTO onlineGameClan) {
      return this.numberOfPlayers == onlineGameClan.getNumberOfPlayers() && this.points == onlineGameClan.getPoints();
    }
    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(numberOfPlayers, points);
  }
}
