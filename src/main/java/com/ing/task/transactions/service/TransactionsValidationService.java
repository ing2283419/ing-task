package com.ing.task.transactions.service;

import static io.netty.util.internal.StringUtil.length;

import com.ing.task.transactions.dto.input.TransactionInputDTO;
import java.math.BigDecimal;

public class TransactionsValidationService {

  private static final int BANK_ACCOUNT_NUMBER_LENGTH = 26;
  private static final String BANK_ACCOUNT_NUMBER_LENGTH_ERROR_MSG = String.format("The bank account number should be %d characters long.", BANK_ACCOUNT_NUMBER_LENGTH);

  private static final String AMOUNT_ERROR_MSG = "The transaction value should be other than null.";

  /**
   * Metoda sprawdza, czy podana transakcja jest prawidłowa.
   * Czyli numery kont mają określoną ilość znaków oraz wartość transakcji jest ustawiona.
   * Jeżeli numer konta ma inną długość niż {@link com.ing.task.transactions.service.TransactionsValidationService.BANK_ACCOUNT_NUMBER_LENGTH} zostanie wyrzucony wyjątek.
   * Jeżeli wartość transakcji nie jest ustawiona (== null) zostanie wyrzucony wyjątek.
   *
   * @param transactionInput - transakcja do sprawdzenia.
   */
  static void validateTransaction(TransactionInputDTO transactionInput) {
    validateAccountNumberLength(transactionInput.debitAccount());
    validateAccountNumberLength(transactionInput.creditAccount());
    validateAmount(transactionInput.amount());
  }

  private static void validateAccountNumberLength(final String account) {
    if (length(account) != BANK_ACCOUNT_NUMBER_LENGTH) {
      throw new RuntimeException(BANK_ACCOUNT_NUMBER_LENGTH_ERROR_MSG);
    }
  }

  private static void validateAmount(final BigDecimal amount) {
    if (amount == null) {
      throw new RuntimeException(AMOUNT_ERROR_MSG);
    }
  }

}
