package com.ing.task.onlinegame.service;

import static com.ing.task.onlinegame.service.OnlineGameServiceHelper.addClanToGroup;
import static com.ing.task.onlinegame.service.OnlineGameServiceHelper.canAddClanToGroup;
import static com.ing.task.onlinegame.service.OnlineGameServiceHelper.groupIsNotFull;
import static com.ing.task.onlinegame.service.OnlineGameServiceHelper.validateAndGroupClans;
import static com.ing.task.onlinegame.service.OnlineGameValidationService.validateMaxGroupCount;
import static io.micronaut.core.util.CollectionUtils.isNotEmpty;
import static java.util.Collections.emptyList;

import com.ing.task.onlinegame.dto.helper.OnlineGameClansWithCountDTO;
import com.ing.task.onlinegame.dto.input.OnlineGameInputDTO;
import com.ing.task.onlinegame.dto.output.OnlineGameOutputDTO;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class OnlineGameService {

  /**
   * Metoda grupuje klany od najmocniejszego do najsłabszego na podstawie ilości punktów w klanie oraz jego wielkości.
   *
   * @param players - lista klanów do pogrupowania oraz informacja o maksymalnej wielkość grupy.
   * @return listę grup (list) klanów, posortowaną według reguł.
   */
  public static List<List<OnlineGameOutputDTO>> calculateGroupsOrder(final OnlineGameInputDTO players) {
    validateMaxGroupCount(players.groupCount());
    if (isNotEmpty(players.clans())) {
      final List<List<OnlineGameOutputDTO>> output = new LinkedList<>();

      final int maxGroupCount = players.groupCount();
      List<OnlineGameClansWithCountDTO> onlineGameClansWithCount = validateAndGroupClans(players.clans(), maxGroupCount);

      while (!onlineGameClansWithCount.isEmpty()) {
        output.add(createClansGroup(onlineGameClansWithCount.listIterator(), maxGroupCount));
      }

      return output;
    }
    return emptyList();
  }

  private static List<OnlineGameOutputDTO> createClansGroup(
      final Iterator<OnlineGameClansWithCountDTO> iterator,
      final int maxGroupCount
  ) {
    int remainingPlaceInGroup = maxGroupCount;
    final List<OnlineGameOutputDTO> clansGroup = new LinkedList<>();
    while (iterator.hasNext() && groupIsNotFull(remainingPlaceInGroup)) {
      final OnlineGameClansWithCountDTO clansWithCount = iterator.next();
      final int numberOfPlayers = clansWithCount.getNumberOfPlayers();
      if (canAddClanToGroup(numberOfPlayers, remainingPlaceInGroup)) {
        final long numberOfClans = clansWithCount.getNumberOfClans();
        for (int i = 1; i <= numberOfClans; i++) {
          if (canAddClanToGroup(numberOfPlayers, remainingPlaceInGroup)) {
            addClanToGroup(clansGroup, numberOfPlayers, clansWithCount.getPoints());
            remainingPlaceInGroup -= numberOfPlayers;
            if (clansWithCount.getNumberOfClans() == 1) {
              iterator.remove();
            } else {
              clansWithCount.decrementNumberOfClans();
            }
          } else {
            break;
          }
        }
      }
    }
    return clansGroup;
  }
}
