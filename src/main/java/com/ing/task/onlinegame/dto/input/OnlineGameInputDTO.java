package com.ing.task.onlinegame.dto.input;

import io.micronaut.serde.annotation.Serdeable;
import java.util.List;

@Serdeable
public record OnlineGameInputDTO(
    int groupCount,
    List<OnlineGameClanDTO> clans
) {

}
