package com.ing.task.onlinegame.dto.output;

import io.micronaut.serde.annotation.Serdeable;

@Serdeable
public record OnlineGameOutputDTO(
    int numberOfPlayers,
    int points
) {

}
