package com.ing.task.onlinegame.service;

import com.ing.task.onlinegame.dto.input.OnlineGameClanDTO;

class OnlineGameValidationService {

  private final static int MIN_GROUP_COUNT = 1;
  private static final String MIN_GROUP_COUNT_ERROR_MSG = String.format("The minimum group size is %d.", MIN_GROUP_COUNT);
  private final static int MAX_GROUP_COUNT = 1_000;
  private static final String MAX_GROUP_COUNT_ERROR_MSG = String.format("The maximum group size is %d.", MAX_GROUP_COUNT);

  private final static int MAX_CLANS_COUNT = 20_000;
  private static final String MAX_CLANS_COUNT_ERROR_MSG = String.format("The maximum number of clans is %d.", MAX_CLANS_COUNT);

  private final static int MIN_CLAN_COUNT = 1;
  private static final String MIN_CLAN_COUNT_ERROR_MSG = String.format("The minimum clan size is %d.", MIN_CLAN_COUNT);
  private final static int MAX_CLAN_COUNT = 1_000;
  private static final String MAX_CLAN_COUNT_ERROR_MSG = String.format("The maximum clan size is %d.", MAX_CLAN_COUNT);

  private final static int MIN_CLAN_POINTS = 1;
  private static final String MIN_CLAN_POINTS_ERROR_MSG = String.format("The minimum number of points in the clan is %d.", MIN_CLAN_POINTS);
  private final static int MAX_CLAN_POINTS = 1_000_000;
  private static final String MAX_CLAN_POINTS_ERROR_MSG = String.format("The maximum number of points in the clan is %d.", MAX_CLAN_POINTS);

  static void validateMaxGroupCount(final int maxGroupCount) {
    if (maxGroupCount < MIN_GROUP_COUNT) {
      throw new RuntimeException(MIN_GROUP_COUNT_ERROR_MSG);
    }
    if (maxGroupCount > MAX_GROUP_COUNT) {
      throw new RuntimeException(MAX_GROUP_COUNT_ERROR_MSG);
    }
  }

  static void validateClansCount(final long clansCount) {
    if (clansCount > MAX_CLANS_COUNT) {
      throw new RuntimeException(MAX_CLANS_COUNT_ERROR_MSG);
    }
  }

  /**
   * Metoda sprawdza, czy dane dotyczące klanu są poprawne.
   * Czy ilość graczy w klanie jest poprawna, jeżeli nie, zostanie rzucony wyjątek.
   * Czy ilość punktów w klanie jest poprawna, jeżeli nie, zostanie rzucony wyjątek.
   *
   * @param clan - klan do sprawdzenia.
   */
  static void validateOnlineGameClan(final OnlineGameClanDTO clan) {
    validateClanNumberOfPlayers(clan.getNumberOfPlayers());
    validateClanPoints(clan.getPoints());
  }

  private static void validateClanNumberOfPlayers(final int numberOfPlayers) {
    if (numberOfPlayers < MIN_GROUP_COUNT) {
      throw new RuntimeException(MIN_CLAN_COUNT_ERROR_MSG);
    }
    if (numberOfPlayers > MAX_GROUP_COUNT) {
      throw new RuntimeException(MAX_CLAN_COUNT_ERROR_MSG);
    }
  }

  private static void validateClanPoints(final int points) {
    if (points < MIN_CLAN_POINTS) {
      throw new RuntimeException(MIN_CLAN_POINTS_ERROR_MSG);
    }
    if (points > MAX_CLAN_POINTS) {
      throw new RuntimeException(MAX_CLAN_POINTS_ERROR_MSG);
    }
  }

}
