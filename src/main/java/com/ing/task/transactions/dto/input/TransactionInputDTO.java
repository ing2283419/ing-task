package com.ing.task.transactions.dto.input;

import io.micronaut.serde.annotation.Serdeable;
import java.math.BigDecimal;

@Serdeable
public record TransactionInputDTO(
    String debitAccount,  // konto, z którego wysyłamy
    String creditAccount, // konto, na które wysyłamy
    BigDecimal amount     // wartość
) {

}