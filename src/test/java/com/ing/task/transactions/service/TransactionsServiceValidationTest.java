package com.ing.task.transactions.service;

import static com.ing.task.transactions.service.TransactionsService.createReport;
import static java.math.BigDecimal.valueOf;
import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.ing.task.transactions.dto.input.TransactionInputDTO;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;

@MicronautTest
class TransactionsServiceValidationTest {

  private final static String DEBIT_ACCOUNT = "PL123456789012345678901234";
  private final static String CREDIT_ACCOUNT = "DE123456789012345678901234";
  private final static BigDecimal AMOUNT = valueOf(12.85);

  private final static String BANK_ACCOUNT_NUMBER_LENGTH_ERROR_MSG = "The bank account number should be 26 characters long.";

  @Test
  void createReportShouldThrowExceptionDebitAccountNull() {
    // given
    List<TransactionInputDTO> transactions = of(new TransactionInputDTO(null, CREDIT_ACCOUNT, AMOUNT));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> createReport(transactions));
    // then
    assertEquals(BANK_ACCOUNT_NUMBER_LENGTH_ERROR_MSG, thrown.getMessage());
  }

  @Test
  void createReportShouldThrowExceptionDebitAccountShorterThan26Characters() {
    // given
    List<TransactionInputDTO> transactions = of(new TransactionInputDTO("PL235", CREDIT_ACCOUNT, AMOUNT));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> createReport(transactions));
    // then
    assertEquals(BANK_ACCOUNT_NUMBER_LENGTH_ERROR_MSG, thrown.getMessage());
  }

  @Test
  void createReportShouldThrowExceptionDebitAccountLongerThan26Characters() {
    // given
    List<TransactionInputDTO> transactions = of(new TransactionInputDTO("PL1234567890123456789012345", CREDIT_ACCOUNT, AMOUNT));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> createReport(transactions));
    // then
    assertEquals(BANK_ACCOUNT_NUMBER_LENGTH_ERROR_MSG, thrown.getMessage());
  }

  @Test
  void createReportShouldThrowExceptionCreditAccountNull() {
    // given
    List<TransactionInputDTO> transactions = of(new TransactionInputDTO(DEBIT_ACCOUNT, null, AMOUNT));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> createReport(transactions));
    // then
    assertEquals(BANK_ACCOUNT_NUMBER_LENGTH_ERROR_MSG, thrown.getMessage());
  }

  @Test
  void createReportShouldThrowExceptionCreditAccountShorterThan26Characters() {
    // given
    List<TransactionInputDTO> transactions = of(new TransactionInputDTO(DEBIT_ACCOUNT, "PL235", AMOUNT));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> createReport(transactions));
    // then
    assertEquals(BANK_ACCOUNT_NUMBER_LENGTH_ERROR_MSG, thrown.getMessage());
  }

  @Test
  void createReportShouldThrowExceptionCreditAccountLongerThan26Characters() {
    // given
    List<TransactionInputDTO> transactions = of(new TransactionInputDTO(DEBIT_ACCOUNT, "PL1234567890123456789012345", AMOUNT));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> createReport(transactions));
    // then
    assertEquals(BANK_ACCOUNT_NUMBER_LENGTH_ERROR_MSG, thrown.getMessage());
  }

  @Test
  void createReportShouldThrowExceptionAmountNull() {
    // given
    List<TransactionInputDTO> transactions = of(new TransactionInputDTO(DEBIT_ACCOUNT, CREDIT_ACCOUNT, null));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> createReport(transactions));
    // then
    assertEquals("The transaction value should be other than null.", thrown.getMessage());
  }

}