package com.ing.task.transactions.service;

import static com.ing.task.transactions.service.TransactionsValidationService.validateTransaction;
import static io.micronaut.core.util.CollectionUtils.isNotEmpty;
import static java.util.Collections.emptyList;

import com.ing.task.transactions.dto.input.TransactionInputDTO;
import com.ing.task.transactions.dto.output.TransactionOutputDTO;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TransactionsService {

  /**
   * Metoda przetwarza wszystkie podane transakcje i zwraca posortowaną listę rachunków, wraz z ilością uznań i obciążeń oraz saldo końcowe.
   *
   * @param transactions - lista transakcji do przetworzenia.
   * @return posortowania lista rachunków, wraz z ilością uznań i obciążeń oraz saldo końcowe.
   */
  public static Collection<TransactionOutputDTO> createReport(final List<TransactionInputDTO> transactions) {
    if (isNotEmpty(transactions)) {
      return groupAndCollectTransactions(transactions);
    }
    return emptyList();
  }

  private static Collection<TransactionOutputDTO> groupAndCollectTransactions(final List<TransactionInputDTO> transactions) {
    final Map<String, TransactionOutputDTO> result = new TreeMap<>();
    transactions.forEach(transaction -> {
      validateTransaction(transaction);
      calculateDebitTransaction(computeIfAbsent(result, transaction.debitAccount()), transaction.amount());
      calculateCreditTransaction(computeIfAbsent(result, transaction.creditAccount()), transaction.amount());
    });
    return result.values();
  }

  private static void calculateDebitTransaction(final TransactionOutputDTO transactionOutput, final BigDecimal amount) {
    transactionOutput.subtractFromBalance(amount);
    transactionOutput.incrementDebitCount();
  }

  private static void calculateCreditTransaction(final TransactionOutputDTO transactionOutput, final BigDecimal amount) {
    transactionOutput.addToBalance(amount);
    transactionOutput.incrementCreditCount();
  }

  private static TransactionOutputDTO computeIfAbsent(final Map<String, TransactionOutputDTO> result, final String account) {
    return result.computeIfAbsent(account, (__) -> new TransactionOutputDTO(account));
  }

}
