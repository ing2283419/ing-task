package com.ing.task.atm.service;

import static com.ing.task.atm.dto.input.RequestType.STANDARD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.ing.task.atm.dto.input.ServiceTaskInputDTO;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import java.util.List;
import org.junit.jupiter.api.Test;

@MicronautTest
class AtmServiceValidationTest {

  @Test
  void calculateOrderShouldThrowExceptionRequestTypeNull() {
    // given
    final List<ServiceTaskInputDTO> serviceTasks = List.of(new ServiceTaskInputDTO(1, 1, null));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> AtmService.calculateOrder(serviceTasks));
    // then
    assertEquals("The request type should be other than null.", thrown.getMessage());
  }

  @Test
  void calculateOrderShouldThrowExceptionRegionNumberLowerThan1() {
    // given
    final List<ServiceTaskInputDTO> serviceTasks = List.of(new ServiceTaskInputDTO(0, 1, STANDARD));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> AtmService.calculateOrder(serviceTasks));
    // then
    assertEquals("The minimum region number is 1.", thrown.getMessage());
  }

  @Test
  void calculateOrderShouldThrowExceptionRegionNumberBiggerThan9999() {
    // given
    final List<ServiceTaskInputDTO> serviceTasks = List.of(new ServiceTaskInputDTO(10_001, 1, STANDARD));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> AtmService.calculateOrder(serviceTasks));
    // then
    assertEquals("The maximum region number is 9999.", thrown.getMessage());
  }

  @Test
  void calculateOrderShouldThrowExceptionAtmIdLowerThan1() {
    // given
    final List<ServiceTaskInputDTO> serviceTasks = List.of(new ServiceTaskInputDTO(1, 0, STANDARD));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> AtmService.calculateOrder(serviceTasks));
    // then
    assertEquals("The minimum ATM ID is 1.", thrown.getMessage());
  }

  @Test
  void calculateOrderShouldThrowExceptionAtmIdBiggerThan9999() {
    // given
    final List<ServiceTaskInputDTO> serviceTasks = List.of(new ServiceTaskInputDTO(1, 10_001, STANDARD));
    // when
    RuntimeException thrown = assertThrows(RuntimeException.class, () -> AtmService.calculateOrder(serviceTasks));
    // then
    assertEquals("The maximum ATM ID is 9999.", thrown.getMessage());
  }

}